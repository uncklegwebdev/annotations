<?php

namespace Cubes\Annotation;

use Cubes\Annotation\Exception\RequiredParameterNotFound;
use Doctrine\Common\Annotations\AnnotationReader;

/**
 * Class Reflector
 *
 * @package Cubes\Annotation
 */
class Reflector extends AnnotationReader implements ReflectorInterface
{
    const REFLECT_CLASS  = 'class';
    const REFLECT_METHOD = 'method';

    /**
     * Array of reflected class document block information.
     *
     * @var array
     */
    protected $reflectedData = [];

    /**
     * Reflector constructor.
     */
    public function __construct()
    {
        Registry::autoload();
        parent::__construct();
    }

    /**
     * @param  string|object  $class
     * @param  string         $method
     *
     * @throws RequiredParameterNotFound
     * @throws \Exception
     *
     * @return mixed
     */
    public function reflect($class, $method = null)
    {
        if (empty($class)) {
            $ex = new RequiredParameterNotFound('subject');
            throw $ex;
        }

        if (!class_exists(is_object($class) ? get_class($class) : $class)) {
            $ex = new \Exception('Class: ' .$class. ' not found or could not be auto-loaded.');
            throw $ex;
        }

        $reflectedClass = new \ReflectionClass($class);
        $this->reflectedData[self::REFLECT_CLASS] = $this->getClassAnnotations($reflectedClass);
        if (!empty($method)) {
            $reflectedMethod = new \ReflectionMethod($class, $method);
            $this->reflectedData[self::REFLECT_METHOD] = $this->getMethodAnnotations($reflectedMethod);
        }

        return $this->reflectedData;
    }
}