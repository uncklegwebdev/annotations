<?php

namespace Cubes\Annotation\Authorization\Token;

/**
 * Class Token
 *
 * @package Cubes\Annotation\Authorization\Token
 */
class Token implements TokenInterface
{
    protected $roles = [];

    /**
     * Token constructor.
     *
     * @param array $roles
     */
    public function __construct(array $roles)
    {
        $this->roles = $roles;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->roles;
    }
}