<?php

namespace Cubes\Annotation\Authorization\Token;

/**
 * Interface TokenInterface
 *
 * @package Cubes\Annotation\Authorization\Token
 */
interface TokenInterface
{
    public function getRoles();
}