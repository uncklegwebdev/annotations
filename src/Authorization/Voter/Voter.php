<?php

namespace Cubes\Annotation\Authorization\Voter;

use Cubes\Annotation\Authorization\Constraint\ConstraintCollectorInterface;
use Cubes\Annotation\Authorization\Token\TokenInterface;

/**
 * Class Voter
 *
 * @package Cubes\Annotation\Authorization\Voter
 */
class Voter implements VoterInterface
{
    /**
     * @var boolean $continue
     */
    protected $continue;

    /**
     * @param  TokenInterface               $token
     * @param  ConstraintCollectorInterface $constraintCollection
     * @return mixed
     */
    public function vote(TokenInterface $token, ConstraintCollectorInterface $constraintCollection)
    {
        $vote = Voter::ACCESS_DENIED;

        $roles = $token->getRoles();
        $constraints = $constraintCollection->getConstraints();

        return $vote;
    }
}