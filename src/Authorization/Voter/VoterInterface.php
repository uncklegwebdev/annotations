<?php

namespace Cubes\Annotation\Authorization\Voter;

use Cubes\Annotation\Authorization\Constraint\ConstraintCollectorInterface;
use Cubes\Annotation\Authorization\Token\TokenInterface;

/**
 * Interface VoterInterface
 *
 * @package Cubes\Annotation\Authorization\Voter
 */
interface VoterInterface
{
    const ACCESS_GRANTED = 1;
    const ACCESS_DENIED  = 0;

    /**
     * @param  TokenInterface               $token
     * @param  ConstraintCollectorInterface $constraintCollection
     * @return mixed
     */
    public function vote(
        TokenInterface $token,
        ConstraintCollectorInterface $constraintCollection
    );
}