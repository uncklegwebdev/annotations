<?php

namespace Cubes\Annotation\Authorization\Annotations;

use Doctrine\Common\Annotations\Annotation;

/**
 *
 * @Annotation
 * @Annotation\Target({"CLASS", "METHOD"})
 */
class Acl
{
    /**
     *
     * @var array
     */
    public $allow;

    /**
     * @var array
     */
    public $deny;

    /**
     * @return array
     */
    public function getAllow()
    {
        return $this->allow;
    }

    /**
     * @return array
     */
    public function getDeny()
    {
        return $this->deny;
    }
}