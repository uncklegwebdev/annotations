<?php

namespace Cubes\Annotation\Authorization;

use Cubes\Annotation\Authorization\Constraint\ConstraintCollector;
use Cubes\Annotation\Authorization\Token\TokenInterface;
use Cubes\Annotation\Authorization\User\UserInterface;
use Cubes\Annotation\Authorization\Voter\Voter;
use Cubes\Annotation\Authorization\Voter\VoterInterface;

/**
 * Class Authorization
 *
 * @package Cubes\Annotation\Authorization
 */
class Authorization implements AuthorizationInterface
{
    /**
     * @var UserInterface $user
     */
    protected $user;

    /**
     * @var string|object $class
     */
    protected $class;

    /**
     * @var string
     */
    protected $method = null;

    /**
     * @var TokenInterface $token
     */
    protected $token;

    /**
     * Authorization constructor.
     *
     * @param UserInterface $user
     * @param string|object $class
     * @param null $method
     */
    public function __construct(UserInterface $user, $class, $method = null)
    {
        $this->user   = $user;
        $this->class  = $class;
        $this->method = $method;
        $this->token  = $this->user->detokenize();
    }

    /**
     * @return boolean
     */
    public function isGranted()
    {
        $class  = $this->class;
        $method = $this->method;

        $voter  = new Voter();
        $vote   = $voter->vote(
            $this->token,
            new ConstraintCollector($class, $method)
        );

        if ($vote == VoterInterface::ACCESS_GRANTED) {
            return true;
        }
    }
}