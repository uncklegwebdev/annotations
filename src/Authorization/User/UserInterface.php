<?php

namespace Cubes\Annotation\Authorization\User;

use Cubes\Annotation\Authorization\Token\TokenInterface;

/**
 * Interface UserInterface
 *
 * @package Cubes\Annotation\Authorization\User
 */
interface UserInterface
{
    /**
     * @return mixed
     */
    public function getRoles();

    /**
     * @return void
     */
    public function tokenize();

    /**
     * @return TokenInterface
     */
    public function detokenize();
}