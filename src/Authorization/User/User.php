<?php

namespace Cubes\Annotation\Authorization\User;

use Cubes\Annotation\Authorization\Token\Token;
use Cubes\Annotation\Authorization\Token\TokenInterface;

/**
 * Class User
 *
 * @package Cubes\Annotation\Authorization\User
 */
class User implements UserInterface
{
    /**
     * @var mixed|null
     */
    protected $user  = null;

    /**
     * @var array
     */
    protected $roles = [];

    /**
     * @var TokenInterface $token
     */
    protected $token;

    /**
     * User constructor.
     *
     * @param mixed $user
     * @param array $roles
     */
    public function __construct($user, array $roles = [])
    {
        $this->user  = $user;
        $this->roles = $roles;
        $this->tokenize();
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Method tokenize used to generate token with required parameters.
     */
    public function tokenize()
    {
        $this->token = new Token(
            $this->getRoles()
        );
    }

    /**
     * @return TokenInterface
     */
    public function detokenize()
    {
        return $this->token;
    }
}