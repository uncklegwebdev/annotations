<?php

namespace Cubes\Annotation\Authorization;


/**
 * Class AuthorizationInterface
 *
 * @package Cubes\Annotation\Authorization
 */
interface AuthorizationInterface
{
    /**
     * @return boolean
     */
    public function isGranted();
}