<?php

namespace Cubes\Annotation\Authorization\Constraint;

use Cubes\Annotation\Reflector;
use Cubes\Annotation\ReflectorInterface;

/**
 * Class ConstraintCollector
 *
 * @package Cubes\Annotation\Authorization\Constraint
 */
class ConstraintCollector implements ConstraintCollectorInterface
{
    /**
     * @var ReflectorInterface $reflector
     */
    protected $reflector;

    /**
     * @var array
     */
    protected $constraints;

    /**
     * ConstraintCollector constructor.
     *
     * @param string|object $class
     * @param string        $method
     */
    public function __construct($class, $method)
    {
        $this->reflector = new Reflector();
        $this->collect($class, $method);
    }

    /**
     * @param string|object $class
     * @param string        $method
     * @return void
     */
    public function collect($class, $method)
    {
        $this->constraints = $this->reflector->reflect(
            $class, $method
        );
    }

    /**
     * @return mixed
     */
    public function getConstraints()
    {
        return $this->constraints;
    }
}