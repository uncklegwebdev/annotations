<?php

namespace Cubes\Annotation\Authorization\Constraint;

/**
 * Interface ConstraintCollectorInterface
 *
 * @package Cubes\Annotation\Authorization\Constraint
 */
interface ConstraintCollectorInterface
{
    public function getConstraints();
}