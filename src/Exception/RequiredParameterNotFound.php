<?php

namespace Cubes\Annotation\Exception;

/**
 * Class RequiredParameterNotFound
 *
 * @package Cubes\Annotation\Exception
 */
class RequiredParameterNotFound extends \Exception
{
    /**
     * RequiredParameterNotFound constructor.
     *
     * @param  string $parameterName
     * @throws RequiredParameterNotFound
     */
    public function __construct($parameterName)
    {
        $ex = new self('Parameter with name: ' . (string) $parameterName . ' is required but not passed.');
        throw $ex;
    }
}