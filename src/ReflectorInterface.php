<?php

namespace Cubes\Annotation;

/**
 * Interface ReflectorInterface
 *
 * @package Cubes\Annotation
 */
interface ReflectorInterface
{
    /**
     * @param string|object $class
     * @param string        $method
     * @return mixed
     */
    public function reflect($class, $method = null);
}