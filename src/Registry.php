<?php

namespace Cubes\Annotation;

use Doctrine\Common\Annotations\AnnotationRegistry;

/**
 * Class Registry
 *
 * @package Cubes\Annotation
 */
final class Registry
{
    /**
     * Array of registered annotation classed for doctrine AnnotationRegistry.
     *
     * @var array
     */
    protected static $autoloader =
        __DIR__ . '/../../../vendor/autoload.php'
    ;

    /**
     * Method registerClasses used to push class file to doctrine
     * AnnotationRegistry so he can autoload it.
     */
    public static function autoload()
    {
        $loader = require self::$autoloader;
        AnnotationRegistry::registerLoader([
            $loader, 'loadClass'
        ]);
    }
}