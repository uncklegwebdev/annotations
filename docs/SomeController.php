<?php

namespace App\Http\Controllers;

use Cubes\Annotation\Authorization\Annotations\Acl;
use Cubes\Annotation\Authorization\Authorization;
use Cubes\Annotation\Authorization\User\User;

/**
 * @Acl(allow={"admin", "moderator"}, deny={"author"})
 */
class IndexController extends Controller
{
    /**
     * @Acl(allow={"admin"})
     */
    public function index()
    {
        $user = new User([], ['moderator']);
        $authorization = new Authorization($user, self::class, 'index');
        if (!$authorization->isGranted()) {
            redirect()->to()->someAction();
        }
    }

    /**
     * @Acl(allow={"*"})
     */
    public function edit()
    {

    }
}